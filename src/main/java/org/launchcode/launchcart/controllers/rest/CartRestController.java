package org.launchcode.launchcart.controllers.rest;

import io.swagger.annotations.ApiOperation;
import org.launchcode.launchcart.data.CartRepository;
import org.launchcode.launchcart.models.Cart;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/carts")
public class CartRestController {

    @Autowired
    CartRepository cartRepository;

    @GetMapping
    public List<Cart> getAllCarts() {
        return cartRepository.findAll();
    }

    @GetMapping("/{id}")
    public Cart getSingleCart(@PathVariable("id") int id) {
        return cartRepository.findOne(id);
    }

    @PutMapping(value = "/{id}")
    @ApiOperation(value = "Modifies and existing Cart",
            notes = "Will replace the cart with the supplied id",
            response = Cart.class,
            responseContainer = "List")
    public ResponseEntity updateCart(@PathVariable("id") int id, @RequestBody Cart cart) {
        if (cart.getUid() != id) {
            return new ResponseEntity("No cart found for ID" + id, HttpStatus.NOT_FOUND);
        }
        cartRepository.save(cart);
        return new ResponseEntity(cart, HttpStatus.OK);
    }


}
