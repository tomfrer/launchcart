package org.launchcode.launchcart.controllers.rest;

import org.launchcode.launchcart.data.CartRepository;
import org.launchcode.launchcart.data.CustomerRepository;
import org.launchcode.launchcart.models.Cart;
import org.launchcode.launchcart.models.Customer;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/customers")
public class CustomerRestController {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    CartRepository cartRepository;

    @GetMapping
    public List<Customer> getCustomers (@RequestParam(required = false) Integer id) {
        if (id == null){
            //both null return all items
            return customerRepository.findAll();
        }else{
            //newParam is null; price is not - query by price
            return customerRepository.findByUid(id);
        }
    }

    @GetMapping("/{id}")
    public Customer getSingleCustomer(@PathVariable("id") int id) {
        return customerRepository.findOne(id);
    }

    @GetMapping("/{id}/cart")
    public List<Cart> getSingleCustomerCart(@PathVariable("id") int id) {
        return cartRepository.findByUid(id);
    }

}
