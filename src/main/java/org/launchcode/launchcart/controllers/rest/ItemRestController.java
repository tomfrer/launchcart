package org.launchcode.launchcart.controllers.rest;

import io.swagger.annotations.ApiOperation;
import org.launchcode.launchcart.data.ItemRepository;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by LaunchCode
 */
@RestController
@RequestMapping("/api/items")
public class ItemRestController {

    @Autowired
    ItemRepository itemRepository;

    @GetMapping
    public List<Item> getItems(@RequestParam(required = false) Double price,
                               @RequestParam(required = false, name="new") Boolean newParam) {

        if (newParam == null) {
            if (price == null){
                //both null return all items
                return itemRepository.findAll();
            }else{
                //newParam is null; price is not - query by price
                return itemRepository.findByPrice(price);
            }
        }else { //newPram is not null
            if (price == null){
                //price is null; newParam is not - query by price
                return itemRepository.findByNewItem(newParam);
            }else{
                //neither is null - query by both
                return itemRepository.findByPriceAndNewItem(price, newParam);
            }
        }

    }

    @GetMapping("/{id}")
    public Item getSingleItem(@PathVariable("id") int id) {
        return itemRepository.findOne(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Item addItem(@RequestBody Item item) {
        return itemRepository.save(item);
    }

    @PutMapping(value = "/{id}")
    @ApiOperation(value = "Modifies and existing Item",
            notes = "Will replace the item with the supplied id",
            response = Item.class,
            responseContainer = "List")
    public ResponseEntity updateItem(@PathVariable("id") int id, @RequestBody Item item) {
        if (item.getUid() != id) {
            return new ResponseEntity("No item found for ID" + id, HttpStatus.NOT_FOUND);
        }
        itemRepository.save(item);
        return new ResponseEntity(item, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteItem(@PathVariable("id") int id) {
        Item itemToDelete = itemRepository.findOne(id);
        if (itemToDelete == null) {
            return new ResponseEntity("No item found for ID" + id, HttpStatus.NOT_FOUND);
        }
        itemRepository.delete(id);
        return new ResponseEntity(id, HttpStatus.OK);
    }

}
