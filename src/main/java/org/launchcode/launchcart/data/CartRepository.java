package org.launchcode.launchcart.data;

import org.launchcode.launchcart.models.Cart;
import org.launchcode.launchcart.models.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by LaunchCode
 */
@Repository
public interface CartRepository extends JpaRepository<Cart, Integer> {

    List<Cart> findByUid(Integer id);

}
