package org.launchcode.launchcart.data;

import org.launchcode.launchcart.models.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by LaunchCode
 */
@Repository
public interface ItemRepository extends JpaRepository<Item, Integer> {

    List<Item> findByPrice(double price);
    List<Item> findByNewItem(boolean newItem);
    List<Item> findByPriceAndNewItem(Double price, boolean newItem);

//    @Query("SELECT * FROM item i WHERE i.price = ?1 and i.new_item = ?2")
//    List<Item> findCustomByPriceAndNew(Double price, boolean newItem);

}
