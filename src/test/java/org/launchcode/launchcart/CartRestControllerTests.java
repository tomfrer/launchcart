package org.launchcode.launchcart;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.launchcart.data.CartRepository;
import org.launchcode.launchcart.data.CustomerRepository;
import org.launchcode.launchcart.models.Cart;
import org.launchcode.launchcart.models.Customer;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class CartRestControllerTests extends AbstractBaseRestIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Before
    public void createTestItems() {
        Customer customer1 = new Customer("cust1", "abc123");
        Customer customer2 = new Customer("cust2", "abc123");
        Item item1 = new Item("nut",0.17);
        Item item2 = new Item("bolt",0.21);
        Item item3 = new Item("washer",0.09);

        Cart cart1 = new Cart();
        cart1.addItem(item1);
        cart1.addItem(item2);
        customer1.setCart(cart1);
        customerRepository.save(customer1);

        Cart cart2 = new Cart();
        cart2.addItem(item3);
        customer2.setCart(cart2);
        customerRepository.save(customer2);
    }

    @Test
    public void testGetAllCarts() throws Exception {
        List<Cart> carts = cartRepository.findAll();
        ResultActions res = mockMvc.perform(get("/api/carts"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(carts.size())));
        for (int i=0; i < carts.size(); i++) {
            res.andExpect(jsonPath("$["+i+"].uid", is(carts.get(i).getUid())));
        }
    }

    @Test
    public void testGetSingleCart() throws Exception {
        Cart aCart = cartRepository.findAll().get(0);
        mockMvc.perform(get("/api/carts/{id}", aCart.getUid()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("uid").value(aCart.getUid()));
    }

    @Test
    public void testPutCart() throws Exception {
        //get a cart
        Cart aCart = cartRepository.findAll().get(0);
        //modify the first item
        String newName = aCart.getItems().get(0).getName() + "xyz";
        aCart.getItems().get(0).setName(newName);
        //remove the second item
        aCart.getItems().remove(1);

        //create JSON
        String json = json(aCart);
        //the resulting JSON should only contain one item and the item should
        //contain the updated name
        mockMvc.perform(put("/api/carts/{id}", aCart.getUid())
                .content(json)
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.items[*]", hasSize(aCart.getItems().size())))
                .andExpect(jsonPath("$.items[0].name").value(newName));

    }

}
