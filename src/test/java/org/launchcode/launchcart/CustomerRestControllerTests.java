package org.launchcode.launchcart;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.launchcart.AbstractBaseRestIntegrationTest;
import org.launchcode.launchcart.IntegrationTestConfig;
import org.launchcode.launchcart.data.CustomerRepository;
import org.launchcode.launchcart.models.Cart;
import org.launchcode.launchcart.models.Customer;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class CustomerRestControllerTests extends AbstractBaseRestIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CustomerRepository customerRepository;

    @Before
    public void createTestItems() {
        Customer customer1 = new Customer("cust1", "abc123");
        Customer customer2 = new Customer("cust2", "abc123");
        Item item1 = new Item("nut",0.17);
        Item item2 = new Item("bolt",0.21);
        Item item3 = new Item("washer",0.09);

        Cart cart1 = new Cart();
        cart1.addItem(item1);
        cart1.addItem(item2);
        customer1.setCart(cart1);
        customerRepository.save(customer1);

        Cart cart2 = new Cart();
        cart2.addItem(item3);
        customer2.setCart(cart2);
        customerRepository.save(customer2);
    }

    @Test
    public void testGetAllCustomers() throws Exception {
        List<Customer> customers = customerRepository.findAll();
        ResultActions res = mockMvc.perform(get("/api/customers"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(customers.size())));
        for (int i = 0; i < customers.size(); i++) {
            res.andExpect(jsonPath("$[" + i + "].uid", is(customers.get(i).getUid())));
        }

    }

    @Test
    public void testGetSingleCustomer() throws Exception {
        Customer aCustomer = customerRepository.findAll().get(0);
        mockMvc.perform(get("/api/customers/{id}", aCustomer.getUid()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("uid").value(aCustomer.getUid()));
    }

    @Test
    public void testGetCartForSingleCustomer() throws Exception {
        Customer aCustomer = customerRepository.findAll().get(0);
        Cart aCart = aCustomer.getCart();
        mockMvc.perform(get("/api/customers/{id}/cart", aCustomer.getUid()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$[0].uid").value(aCart.getUid()));
    }

}